package com.ws.server.endpoint;

import com.ws.server.model.Country;
import com.ws.server.model.GetCountryRequest;
import com.ws.server.model.GetCountryResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class CountryEndpoint {




        public static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";


        @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCountryRequest")
        @ResponsePayload
        public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) {
            GetCountryResponse response = new GetCountryResponse();
            Country country = new Country();
            country.setName("client传参："+request.getName());
            response.setCountry(country );
            return response;
        }
    }