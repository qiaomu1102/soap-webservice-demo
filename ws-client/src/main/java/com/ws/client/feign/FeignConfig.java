package com.ws.client.feign;

import feign.*;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * ---------------------------------------------------------------
 * Author: perseverance_li
 * Email: perseverance_li@126.com
 * Create: 2019-12-11 15:53
 * ---------------------------------------------------------------
 * Describe:
 * ---------------------------------------------------------------
 * Changes:
 * ---------------------------------------------------------------
 * 2019-12-11 15:53 : Create by perseverance_li
 * ---------------------------------------------------------------
 */
@Slf4j
@Configuration
@ConditionalOnClass(Feign.class)
@AutoConfigureBefore(FeignAutoConfiguration.class)
public class FeignConfig {

    /**
     * 读取超时时间,单位毫秒
     */
    @Value("${okhttp.read-timeout}")
    private int readTimeout;
    /**
     * 写超时,单位毫秒
     */
    @Value("${okhttp.write-timeout}")
    private int writeTimeout;
    /**
     * 连接超时,单位毫秒
     */
    @Value("${okhttp.connection-timeout}")
    private int connectionTimeout;
    /**
     * 线程池最大空闲数
     */
    @Value("${okhttp.max-idle-connections}")
    private int maxIdleConnections;
    /**
     * 每个线程最大空闲时间,单位分钟
     */
    @Value("${okhttp.keep-alive-duration}")
    private int keepAliveDuration;


    /**
     * 开启重试机制
     *
     * @return
     */
    @Bean
    public Retryer feignRetryer() {
        return   Retryer.NEVER_RETRY ;
    }

    /**
     * FULL：记录所有请求与响应的明细，包括头信息、请求体、元数据
     */
    @Bean
    feign.Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    /**
     * 如果不重新设置feign.Request.Options则自定义的超时时间等会被默认的options覆盖
     * <p>
     * 可查看feign.okhttp.OkHttpClient#execute方法
     * <p>
     * {@link feign.Request.Options}
     * {@link feign.okhttp.OkHttpClient#execute(Request, Request.Options)}
     *
     * @return
     */
    @Bean
    public Request.Options feignOptions() {
        return new feign.Request.Options(connectionTimeout, readTimeout);
    }


    /**
     * 如果不注入Client则不会使用okhttp发送请求
     * <p>
     * 可见{@link FeignAutoConfiguration}
     * OkHttpFeignConfiguration -> @ConditionalOnMissingBean(okhttp3.OkHttpClient.class)
     *
     * @param client
     * @return
     */
    @Bean
    @ConditionalOnMissingBean({Client.class})
    public Client feignClient(OkHttpClient client) {
        log.info("feign client : " + client.toString());
        return new feign.okhttp.OkHttpClient(client);
    }

    @Bean
    public OkHttpClient client() {
        ConnectionPool pool = new ConnectionPool(maxIdleConnections, keepAliveDuration, TimeUnit.MINUTES);
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
                .writeTimeout(writeTimeout, TimeUnit.MILLISECONDS)
                .connectTimeout(connectionTimeout, TimeUnit.MILLISECONDS)
                .connectionPool(pool)
                .build();
        log.info("okhttp config : " + client.toString());
        log.info("okhttp config readTimeout : " + readTimeout);
        log.info("okhttp config writeTimeout : " + writeTimeout);
        log.info("okhttp config connectionTimeout : " + connectionTimeout);
        return client;
    }
}
