package com.ws.client.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(
        name = "codeService", url = "http://localhost:8888"
        ,fallbackFactory = CodeServiceFallBack.class
)
public interface CodeService {

    @GetMapping(value = "/generator/list" )
    @ResponseBody
    String list( );
}
