package com.ws.client.feign;

import com.ws.client.model.GetCountryRequest;
import com.ws.client.model.GetCountryResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        name = "soapWSService",
        url = "http://localhost:8080/ws/countries.wsdl",
        configuration =  SoapClientConfiguration.class)
public interface SoapWSService {

    @PostMapping( consumes = MediaType.TEXT_XML_VALUE, produces = MediaType.TEXT_XML_VALUE)
    GetCountryResponse getCountry(@RequestBody GetCountryRequest request );

}
