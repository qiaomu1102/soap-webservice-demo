package com.ws.client.feign;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CodeServiceFallBack implements FallbackFactory<CodeService> {

    @Override
    public CodeService create(Throwable throwable) {
        return new CodeService() {
            @Override
            public String list() {
                log.error("连接超时=====================================================");
                return "请求错误，HTTP连接失败";
            }
        };
    }
}
