package com.ws.client.webservice;

import com.ws.client.model.GetCountryRequest;
import com.ws.client.model.GetCountryResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class CountryClient extends WebServiceGatewaySupport {
    public static final String URI = "http://localhost:8080/ws/countries.wsdl";

    public GetCountryResponse getCountry(String name) {
        GetCountryRequest request = new GetCountryRequest();
        request.setName(name);
        GetCountryResponse response = (GetCountryResponse) getWebServiceTemplate().marshalSendAndReceive( URI, request);
        return response;
    }
}
