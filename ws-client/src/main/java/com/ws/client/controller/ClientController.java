package com.ws.client.controller;

import com.ws.client.feign.CodeService;
import com.ws.client.feign.SoapWSService;
import com.ws.client.model.GetCountryRequest;
import com.ws.client.model.GetCountryResponse;
import com.ws.client.webservice.CountryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class ClientController {


    @Autowired
    private CodeService codeService;

    @Autowired
    private SoapWSService soapWSService;

    @Autowired
    private CountryClient countryClient;


    //普通的feign http使用
    @GetMapping("/code")
    public String getCodeList(){
        return codeService.list();
    }


    //feign 调用soap方式
    @GetMapping("/soap-feign")
    public GetCountryResponse soapFeign(){
        GetCountryRequest request = new GetCountryRequest();
        request.setName("feign参数美国");
        return soapWSService.getCountry(request);
    }

    //spring webservice 调用soap方式
    @GetMapping("/soap-ws")
    public GetCountryResponse soapWS(){
        GetCountryResponse result = countryClient.getCountry("webservice参数马达加斯加");
        return result;
    }

}
